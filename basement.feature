Feature: Find out on what index Santa hits the basement the first time.

    Scenario Outline: ( means santa should go up one floor and ) means he should go down one floor.

        Given The same instructions, find the position of the first character that causes him to enter the basement

        When Santa is following these instructions

        Then Santa will be present in the basement

        Examples:
        
        |must appear |  Result     |
        |(()))       |  place no 5 |   
