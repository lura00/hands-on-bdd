"""AoC 2015 day1 part 1 & 2"""

def get_data():
    with open('aoc_day1.txt', 'r') as parantheses:
        instructions = set(input for input in parantheses.readlines())
    directives = []
    for instruction in instructions:
        directives = instruction
        return directives
list = get_data()

def following_instructions(list):
    count1 = 0
    count2 = 0
    floor = 0
    for instructions in list:
        if instructions == "(":
            count1 += 1
        elif instructions == ")":
            count2 += 1
    floor = count1 - count2
    return floor

print(following_instructions(list))

"""Day 1 part 2"""

def find_basement(list):
    floor = 0

    for i, c in enumerate(list):
        if c == ")":
            floor -= 1
        elif c == "(":
            floor += 1

        if floor == -1:
            print(i+1)
            break
find_basement(list)