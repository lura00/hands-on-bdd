"""AoC 2015 day2 part 1 & 2"""

def area_list():
    file = open("aoc_day2.txt")
    area = file.readlines()
    file.close()
    return area

list = area_list()
#print(list)

def calculate_area(list):
    tot_area = 0
    # print(list)
    for box in list:
        l, w, h = sorted(map(int, box.split('x')))
        
        area = (2 * l * w) + (2 * l * h) + (2 * w * h)
        extra_paper = l * w
        tot_area += (area + extra_paper)
        #print(tot_area)
        return tot_area

calculate_area(list)

def calculate_ribbon(list):
    total_ribbon = 0
    for box in list:
        l, w, h = sorted(map(int, box.split('x')))

        bow = l * w * h
        ribbon = (2*l) + (2*w)
        total_ribbon += (bow + ribbon)
    print(total_ribbon)
    return total_ribbon

calculate_ribbon(list)