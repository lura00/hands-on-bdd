Feature: The elves are also running low on ribbon. 
            Ribbon is all the same width, so they only have to worry about the length they need to order, 
            which they would again like to be exact.

    Scenario Outline: The ribbon required to wrap a present is the shortest distance around its sides

        Given There is a list of box surface area calcualtions

        When We calculate all the shortest side around the box and ad some extra

        Then The elves will order x amount of ribbon

        Examples:
        
        |must appear |  Ribbon needed | Extra | total |
        |2x3x4       |  2+2+3+3       | 2*3*4 | 34    |