Feature: Finding what floor santa is to get off his elevator

    Scenario Outline: ( means santa should go up one floor and ) means he should go down one floor.

        Given There is a list of instructions

        When Santa is following these instructions

        Then Santa will be present at floor x

        Examples:
        
        |must appear |  Result  |
        |(())        |  floor 0 |   
        |(()(()(     |  floor 3 |   
        |(((         |  floor 3 |