"""Find at what index santa hits the basement for the first time (floor -1)"""
"""Find out on what index Santa hits the basement the first time. feature tests."""
import main
from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('basement.feature', '( means santa should go up one floor and ) means he should go down one floor.')
def test__means_santa_should_go_up_one_floor_and__means_he_should_go_down_one_floor():
    """( means santa should go up one floor and ) means he should go down one floor.."""
    pass


@given('The same instructions, find the position of the first character that causes him to enter the basement', target_fixture="list")
def test_the_same_instructions_find_the_position_of_the_first_character_that_causes_him_to_enter_the_basement():
    """The same instructions, find the position of the first character that causes him to enter the basement."""
    return main.get_data()


@when('Santa is following these instructions', target_fixture="result")
def test_santa_is_following_these_instructions(list):
    """Santa is following these instructions."""
    return main.find_basement(list)


@then('Santa will be present in the basement')
def test_santa_will_be_present_in_the_basement(result, list):
    """Santa will be present in the basement."""
    assert result == main.find_basement(list)
