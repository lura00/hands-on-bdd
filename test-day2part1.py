"""The elves are running low on wrapping paper, and so they need to submit an order for more. feature tests."""
import main_day2

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('wrappingPaper.feature', 'Every present is a box, the surface are of the box calculates 2*l*w + 2*w*h + 2*h*l.')
def test_every_present_is_a_box_the_surface_are_of_the_box_calculates_2lw__2wh__2hl():
    """Every present is a box, the surface are of the box calculates 2*l*w + 2*w*h + 2*h*l.."""
    pass


@scenario('wrappingPaper.feature', 'They also need a little extra paper for each present: the area of the smallest side.')
def test_they_also_need_a_little_extra_paper_for_each_present_the_area_of_the_smallest_side():
    """They also need a little extra paper for each present: the area of the smallest side.."""
    pass


@given('There is a list of box surface areas', target_fixture='list')
def test_there_is_a_list_of_box_surface_areas():
    """There is a list of box surface areas."""
    return main_day2.area_list()


@when('We calculate all the dimensions and ad the extra bit of paper', target_fixture='result')
def test_we_calculate_all_the_dimensions_and_ad_the_extra_bit_of_paper(list):
    """We calculate all the dimensions and ad the extra bit of paper."""
    return main_day2.calculate_area(list)


@then('The elves will know how much paper to order')
def test_the_elves_will_know_how_much_paper_to_order(result, list):
    """The elves will know how much paper to order."""
    assert result == main_day2.calculate_area(list)

