"""The elves are also running low on ribbon. feature tests."""
import main_day2
from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('ribbon.feature', 'The ribbon required to wrap a present is the shortest distance around its sides')
def test_the_ribbon_required_to_wrap_a_present_is_the_shortest_distance_around_its_sides():
    """The ribbon required to wrap a present is the shortest distance around its sides."""
    pass


@given('There is a list of box surface area calcualtions', target_fixture='list')
def test_there_is_a_list_of_box_surface_area_calcualtions():
    """There is a list of box surface area calcualtions."""
    return main_day2.area_list()


@when('We calculate all the shortest side around the box and ad some extra', target_fixture='result')
def test_we_calculate_all_the_shortest_side_around_the_box_and_ad_some_extra(list):
    """We calculate all the shortest side around the box and ad some extra."""
    return main_day2.calculate_ribbon(list)


@then('The elves will order x amount of ribbon')
def test_the_elves_will_order_x_amount_of_ribbon(result, list):
    """The elves will order x amount of ribbon."""
    assert result == main_day2.calculate_ribbon(list)
