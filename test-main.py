import main

"""Finding what floor santa is to get off his elevator feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)


@scenario('santadir.feature', '( means santa should go up one floor and ) means he should go down one floor.')
def test__means_santa_should_go_up_one_floor_and__means_he_should_go_down_one_floor():
    """( means santa should go up one floor and ) means he should go down one floor.."""
    pass

# @given(parsers.parse('There is a list of instructions {sequence:s}'), target_fixture='list')
@given('There is a list of instructions', target_fixture='list')
def there_is_a_list_of_instructions():
    """There is a list of instructions."""
    return main.get_data()


@when('Santa is following these instructions', target_fixture='instructions_result')
def santa_is_following_these_instructions(list):
    """Santa is following these instructions."""
    return main.following_instructions(list)


# @then(parsers.parse('Santa will be present at floor {result:d}'))
@then('Santa will be present at floor x')
def santa_will_be_present_at_floor_x(instructions_result, list):
    """Santa will be present at floor x."""
    assert instructions_result == main.following_instructions(list)

