Feature: The elves are running low on wrapping paper, and so they need to submit an order for more. 
            They have a list of the dimensions (length l, width w, and height h) of each present,
             and only want to order exactly as much as they need.

    Scenario Outline: Every present is a box, the surface are of the box calculates 2*l*w + 2*w*h + 2*h*l. 
                        They also need a little extra paper for each present: the area of the smallest side.

        Given There is a list of box surface areas

        When We calculate all the dimensions and ad the extra bit of paper

        Then The elves will know how much paper to order

        Examples:
        
        |must appear |  Paper needed          | Extra | total |
        |2x3x4       |  2*6 + 2*12 + 2*8 = 52 |   6   | 58    |